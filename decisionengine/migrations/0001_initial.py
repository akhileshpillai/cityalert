# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SendAlertFrequency'
        db.create_table(u'decisionengine_sendalertfrequency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subscriber.Contact'])),
            ('call_code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cityalertdata.Call_Code'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cityalertdata.Event'])),
        ))
        db.send_create_signal(u'decisionengine', ['SendAlertFrequency'])


    def backwards(self, orm):
        # Deleting model 'SendAlertFrequency'
        db.delete_table(u'decisionengine_sendalertfrequency')


    models = {
        u'cityalertdata.call_code': {
            'Meta': {'object_name': 'Call_Code'},
            'call_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'call_code_desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part_of_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'cityalertdata.event': {
            'Meta': {'object_name': 'Event'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'full_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'geolocation': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tr': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'decisionengine.sendalertfrequency': {
            'Meta': {'object_name': 'SendAlertFrequency'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscriber.Contact']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'subscriber.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscriber.Subscriber']"})
        },
        u'subscriber.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['decisionengine']
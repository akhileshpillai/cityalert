#standard lib
from __future__ import absolute_import
import logging,sys
import re
import sys
import dateutil.parser


# third party lib
from celery import shared_task
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist 
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.cache import cache
import json
import urllib
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D
from django.db import Error
from django.db import DatabaseError
from django.db import DataError
from datetime import datetime


#application specific
from cityalertdata.models import *
from subscriber.models import *
from decisionengine.models import *



@shared_task(name='decisionengine.tasks.processData')
def processData():
   
    """
        Name: decisionengine.tasks.processData Function: Populates SendAlertFrequency Table which is eventually used by sendalert module.

        1) Get new event from the events table.
        2) Find the relevant alerts that needs to be send out to subscribers based on their preferences of call_code and location of their property.
        3) The relevant alerts are added to SendAlertFrequency table
    
    """
    try:
        #Get another unprocessed event from the events table
        event = get_new_event() 
        if not event:
            raise DataError("No new event")

        event_call_code = event.call_code.call_code 
        
        # find the relevant alerts that needs to be send out to subscribers based on their preferences of call_code and location of their property  
        relevant_alerts = Contact_Pref.objects.filter(contact__address__distance_lte=(event.geolocation,D(mi=0.25)),call_code__call_code=event_call_code) 
        if relevant_alerts:
            print "# of relevant matches: %s for event: %s" % (relevant_alerts.__len__(), event.id)
            for i in relevant_alerts:
                contact_obj= i.contact
                event_obj = event
                call_code = i.call_code 
                date = datetime.today() 
                # The relevant alerts are added to SendAlertFrequency table
                insert_into_subpref(contact_obj,event_obj,call_code, date)
        else:
            event.processed=True
            event.save()
            raise DataError("No subscriber matches the given alert")
    except DataError, e:
        print e
    except Error, e:
        print e
    else:
        event.processed=True
        event.save()
    finally:
        print "Completed processing event event.id %s " % (event.id)
       
def insert_into_subpref(contact_obj,event_obj,call_code, date):
    print " Adding call code to SendAlertFrequency"
    try:
        new_pref = SendAlertFrequency.objects.create(contact=contact_obj,event=event_obj,call_code=call_code, date=date)    
    except DatabaseError:
        raise Error("Can't add the preference %s of contact: %s wrt event : %s on date :%s" % (call_code, contact_obj, event_obj, date))
        

def get_new_event():
    try:
        new_event = Event.objects.filter(processed=False).earliest('id')
        return new_event
    except ObjectDoesNotExist:
        return None 


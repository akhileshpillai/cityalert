from django.contrib.gis.db import models
from subscriber.models import Contact 
from cityalertdata.models import Call_Code
from cityalertdata.models import Event

# Create your models here.
class SendAlertFrequency(models.Model):
    contact = models.ForeignKey(Contact)
    call_code = models.ForeignKey(Call_Code)
    date = models.DateTimeField(blank=False,)  
    event = models.ForeignKey(Event)


from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from subscriber.forms import ContactForm
from django.core.cache import cache
from subscriber.models import Subscriber
from subscriber.models import Contact
from subscriber.models import Contact_Pref 
from cityalertdata.models import Call_Code
from django.contrib.gis.geos import Point
from django.db import transaction
from django.db import DataError
from django.db import DatabaseError
from django.db import NotSupportedError
from django.db import IntegrityError
from django.db import Error

# Create your views here.
def contact(request):
    if request.method == 'POST': # If the form has been submitted...
        data = {}
        print "request.POST : ", request.POST
        data["email"] = request.POST["username"]
        data["lat"] = request.POST["lat"]
        data["lng"] = request.POST["lng"]
        data["phone"] = request.POST["phonenumber"]
        data["preference"] = request.POST.getlist("code") 
        print data["preference"]
        form = ContactForm(data) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            print " form is validated" 
            # Process the data in form.cleaned_data
            # ...
            geo_point =  Point(form.cleaned_data['lat'], form.cleaned_data['lng'])
            geo_point.srid = 32140
            try:
                subscriber_obj, created = Subscriber.objects.get_or_create(email=data["email"],defaults={'email': data["email"]})
                with transaction.atomic():
                    print "Inside Transaction"
                    try:
                    # if the address already exists update contact_pref else create the contact_pref
                        contact_obj, created = Contact.objects.get_or_create(subscriber=subscriber_obj,address=geo_point,defaults={'subscriber': subscriber_obj, 'address' : geo_point, 'phone': form.cleaned_data['phone']})
                        if not created:
                            print "Inside Transaction: not created"
                            contact_pref_obj_del = Contact_Pref.objects.filter(contact=contact_obj)
                            contact_pref_obj_del.delete()
            
                        for i in data["preference"]:
                            print "Inside Transaction: data[preference]"
                            # Update : need to change this cache mechanism ideally cache would be prepopulated with data.
                            cv = cache.get(i)
                            if not cv:
                                print "Not found in cache"
                                call_code_obj = Call_Code.objects.filter(call_code=i)
                                cv = call_code_obj[0]
                                cache.set(i,cv)
                            contact_pref_obj = Contact_Pref.objects.create(contact=contact_obj,call_code=cv)
                    except NotSupportedError: 
                        print "Error notsupported "
                        raise IntegrityError
                    except DatabaseError:
                        print "Error DatabaseError"
                        raise IntegrityError
                    except Error:
                        print "Error exception"
                        raise IntegrityError
                    except DataError:
                        print "unknown exception"
                        raise IntegrityError

            except IntegrityError:
                print " Transaction rolled back"
        
            return HttpResponseRedirect(reverse('home')) # Redirect after POST

        else:
            print form.errors
            return render(request, 'subscriber/preference/settings.html', {
                    'form': form,
                        })
    else:
        print "In Get "
        form = ContactForm() # An unbound form
        ui_cc = cache.get("UI_CALL_CODE") 
        if not ui_cc:
            cc_from_database =[]
            ui_call_code_obj = Call_Code.objects.filter(part_of_ui=True)
            for cc in ui_call_code_obj:
                cc_from_database.append(cc)  
            cache.set("UI_CALL_CODE",cc_from_database,3600)
            ui_cc = cc_from_database
            print "ui_cc from database", ui_cc
        form.ui_call_code = ui_cc      
        print "form.ui_call_code",form.ui_call_code

    return render(request, 'subscriber/settings.html', {
        'form': form,
    })

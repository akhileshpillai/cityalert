from django.contrib.gis.db import models
from cityalertdata.models import Call_Code

# Create your models here.
class Subscriber(models.Model):
    email = models.CharField(max_length=255,)
    objects = models.GeoManager()

class Contact(models.Model):
    subscriber = models.ForeignKey(Subscriber)
    address = models.PointField(srid=32140)
    phone = models.CharField(max_length=255,)
    objects = models.GeoManager()

class Contact_Pref(models.Model):
    contact = models.ForeignKey(Contact)
    call_code = models.ForeignKey(Call_Code)
    objects = models.GeoManager()

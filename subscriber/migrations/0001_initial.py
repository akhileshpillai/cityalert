# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Subscriber'
        db.create_table(u'subscriber_subscriber', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'subscriber', ['Subscriber'])

        # Adding model 'Contact'
        db.create_table(u'subscriber_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('subscriber', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subscriber.Subscriber'])),
            ('address', self.gf('django.contrib.gis.db.models.fields.PointField')(srid=32140)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'subscriber', ['Contact'])

        # Adding model 'Contact_Pref'
        db.create_table(u'subscriber_contact_pref', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subscriber.Contact'])),
            ('call_code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cityalertdata.Call_Code'])),
        ))
        db.send_create_signal(u'subscriber', ['Contact_Pref'])


    def backwards(self, orm):
        # Deleting model 'Subscriber'
        db.delete_table(u'subscriber_subscriber')

        # Deleting model 'Contact'
        db.delete_table(u'subscriber_contact')

        # Deleting model 'Contact_Pref'
        db.delete_table(u'subscriber_contact_pref')


    models = {
        u'cityalertdata.call_code': {
            'Meta': {'object_name': 'Call_Code'},
            'call_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'call_code_desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part_of_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'subscriber.contact': {
            'Meta': {'object_name': 'Contact'},
            'address': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'subscriber': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscriber.Subscriber']"})
        },
        u'subscriber.contact_pref': {
            'Meta': {'object_name': 'Contact_Pref'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subscriber.Contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'subscriber.subscriber': {
            'Meta': {'object_name': 'Subscriber'},
            'email': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['subscriber']
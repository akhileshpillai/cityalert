from django import forms 

class ContactForm(forms.Form):
    lat = forms.FloatField(required=True)
    lng = forms.FloatField(required=True)
    email = forms.EmailField(required=True)
    phone = forms.RegexField(regex='^\d{10,}$',required=True)
    preference = forms.CharField(required=True)
    #preference =  forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple)


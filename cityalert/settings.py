"""
Django settings for cityalert project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
from __future__ import absolute_import
from celery.schedules import  crontab

import dj_database_url
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4t1ayv=7j^(&b4*mkt!0exvi%0z2q_+#2ry+--z+bh*6&%8lro'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.gis',
    'django.contrib.sites',
    'south',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'cityauth',
    'cityalertdata',
    'subscriber',
    'decisionengine',
    'rest_framework',
    'djcelery',
    'sendalert',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'cityalert.urls'

WSGI_APPLICATION = 'cityalert.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases
DATABASES = {
            'default': {
                           'ENGINE': 'django.contrib.gis.db.backends.postgis',
                           'NAME': 'citygeodb',
                           'USER': 'citygeodbuser',
                           'PASSWORD': 'citygeodbuser',
                      }
            }

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
            os.path.join(BASE_DIR, 'static'),
            )
#STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

# all auth settings starts here
AUTHENTICATION_BACKENDS = (
    # Needed to login by username in Django admin, regardless of `allauth`
    "django.contrib.auth.backends.ModelBackend",
    # `allauth` specific authentication methods, such as login by e-mail
    "allauth.account.auth_backends.AuthenticationBackend"
)
 
TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.request",
    "django.contrib.auth.context_processors.auth",
    "allauth.account.context_processors.account",
    "allauth.socialaccount.context_processors.socialaccount",
    "cityauth.context_processors.global_callcode_settings",
)
 
# auth and allauth settings
LOGIN_REDIRECT_URL = '/'
SOCIALACCOUNT_QUERY_EMAIL = True
ACCOUNT_UNIQUE_EMAIL = False 
ACCOUNT_EMAIL_VERIFICATION = "none"
ACCOUNT_CONFIRM_EMAIL_ON_GET = True

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email', 'publish_stream'],
        'METHOD': 'oauth2'  # instead of 'oauth2'
    },
    'google': {
         'SCOPE': ['https://www.googleapis.com/auth/userinfo.profile','email'],
         'AUTH_PARAMS': { 'access_type': 'online' } ,
         'REDIRECT_URL': 'http://cityalert.akhileshpillai.com/accounts/google/login/callback/',      
    }
}

SITE_ID = 1


# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
)

STATICFILES_FINDERS = (
            'django.contrib.staticfiles.finders.FileSystemFinder',
            'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

CALL_CODE = {'code1':"Description for code1",
            'code2':"Description for code2",
            'code3':"Description for code3",
            'code4':"Description for code4",
            }

# **************all auth settings end here *******************

#############################
############################    memcache setting #############################
############################

CACHES = {
            'default': {
                        'BACKEND': 'django_bmemcached.memcached.BMemcached',
                                'LOCATION': os.environ.get('MEMCACHEDCLOUD_SERVERS').split(','),
                                        'OPTIONS': {
                                                                'username': os.environ.get('MEMCACHEDCLOUD_USERNAME'),
                                                                'password': os.environ.get('MEMCACHEDCLOUD_PASSWORD')
                                                   }
                                         }
            }






#CACHES = {
#            'default': {
#                        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
#                        'LOCATION': '127.0.0.1:11211',
#                       }
#         }


################################
###############################    Celery RabbitMQ configuration ###################################
################################

#BROKER_URL = "amqp://cityalertuser:cityalertuser@localhost:5672/cityalerthost"
BROKER_URL = os.environ.get('CLOUDAMQP_URL', 'amqp://cityalertuser:cityalertuser@localhost:5672/cityalerthost')

CELERY_RESULT_BACKEND=os.environ.get('REDISCLOUD_URL','redis://localhost:6379/0')

CELERYBEAT_SCHEDULE = {
                                'get-data-every-5': {
                                                            'task': 'cityalertdata.tasks.getData',
                                                            'schedule': crontab(minute='*/50'),
                                                     },
                                'match-subscriber-2alert': {
                                                            'task': 'decisionengine.tasks.processData',
                                                            'schedule': crontab(minute='*/55'),
                                                     },
                                'daily-alert-sender': {
                                                            'task': 'sendalert.tasks.daily_frequency_alert',
                                                            'schedule': crontab(minute='*/20'),
                                                     },

                      }


#GOOGLE_API_KEY="AIzaSyAfhyEIK6DJpOfuC48iGRY8snWZc2f1hqg"
#CITY_LAT=40.71797910802078
#CITY_LNG=-74.04299139976501



################################
##############################    CITY DATA ( events )  related flags ###############################
###############################
ADDRESS=True
#GOOGLE_API_KEY="AIzaSyAfhyEIK6DJpOfuC48iGRY8snWZc2f1hqg"
GOOGLE_API_KEY="AIzaSyCWOh9LKSGY1qjHb2thqjYnh6OiBKqowQ8"
CITY_LAT=40.71797910802078
CITY_LNG=-74.04299139976501

DATABASES['default'] =  dj_database_url.config()
DATABASES['default']['ENGINE'] = 'django.contrib.gis.db.backends.postgis'

GEOS_LIBRARY_PATH = os.environ.get('GEOS_LIBRARY_PATH')
GDAL_LIBRARY_PATH = os.environ.get('GDAL_LIBRARY_PATH')

from __future__ import absolute_import
import os

from celery import Celery
from django.conf import settings
from celery import shared_task
 
# Indicate Celery to use the default Django settings module
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cityalert.settings')

app = Celery('cityalerts')
app.config_from_object('django.conf:settings')
# This line will tell Celery to autodiscover all your tasks.py that are in your app folders
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

@shared_task()
def test_debug_task():
    print("in test debug")

@app.task()
def debug_task():
    print("in debug_task")


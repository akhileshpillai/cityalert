from django.conf.urls import patterns, include, url
from django.contrib import admin
import cityauth
import subscriber
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout',{'next_page': '/'}),
    url(r'^accounts/', include('allauth.urls')),
    #url(r'^alert/preference/','cityauth.views.user_settings'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^subscriber/preference/$', 'subscriber.views.contact'),
    url(r'^$', 'cityauth.views.index', name='home')
)


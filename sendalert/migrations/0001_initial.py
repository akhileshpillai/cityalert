# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'setting'
        db.create_table(u'sendalert_setting', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('account_sid', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('auth_token', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('fromph', self.gf('phonenumber_field.modelfields.PhoneNumberField')(max_length=128)),
        ))
        db.send_create_signal(u'sendalert', ['setting'])


    def backwards(self, orm):
        # Deleting model 'setting'
        db.delete_table(u'sendalert_setting')


    models = {
        u'sendalert.setting': {
            'Meta': {'object_name': 'setting'},
            'account_sid': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'auth_token': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'fromph': ('phonenumber_field.modelfields.PhoneNumberField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['sendalert']
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'setting', fields ['account_sid']
        db.delete_unique(u'sendalert_setting', ['account_sid'])

        # Removing unique constraint on 'setting', fields ['auth_token']
        db.delete_unique(u'sendalert_setting', ['auth_token'])

        # Adding field 'setting.vendor'
        db.add_column(u'sendalert_setting', 'vendor',
                      self.gf('django.db.models.fields.CharField')(default='twilio', unique=True, max_length=255),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'setting.vendor'
        db.delete_column(u'sendalert_setting', 'vendor')

        # Adding unique constraint on 'setting', fields ['auth_token']
        db.create_unique(u'sendalert_setting', ['auth_token'])

        # Adding unique constraint on 'setting', fields ['account_sid']
        db.create_unique(u'sendalert_setting', ['account_sid'])


    models = {
        u'sendalert.setting': {
            'Meta': {'object_name': 'setting'},
            'account_sid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'auth_token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fromph': ('phonenumber_field.modelfields.PhoneNumberField', [], {'max_length': '128'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vendor': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['sendalert']
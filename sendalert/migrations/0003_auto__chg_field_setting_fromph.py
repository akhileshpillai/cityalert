# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'setting.fromph'
        db.alter_column(u'sendalert_setting', 'fromph', self.gf('django.db.models.fields.CharField')(max_length=255))

    def backwards(self, orm):

        # Changing field 'setting.fromph'
        db.alter_column(u'sendalert_setting', 'fromph', self.gf('phonenumber_field.modelfields.PhoneNumberField')(max_length=128))

    models = {
        u'sendalert.setting': {
            'Meta': {'object_name': 'setting'},
            'account_sid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'auth_token': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'fromph': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vendor': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        }
    }

    complete_apps = ['sendalert']
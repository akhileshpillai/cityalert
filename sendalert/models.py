from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.core.cache import get_cache
from django.core.cache import cache
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class setting(models.Model):
    account_sid = models.CharField(max_length=255,) 
    auth_token = models.CharField(max_length=255,)
    fromph= models.CharField(max_length=255,)
    vendor = models.CharField(max_length=255,unique=True) 
    
    def __unicode__(self):
        name = "#account_sid:  " +str(self.account_sid) + "  #auth_token: " + str(self.auth_token) + " #vendor: " + str(self.vendor)
        return name


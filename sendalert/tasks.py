#standard lib
from __future__ import absolute_import
import logging,sys
import re
import sys
import dateutil.parser


# third party lib
from celery import shared_task
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist 
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.cache import cache
import json
import urllib
from django.contrib.gis.geos import *
from django.contrib.gis.measure import D
from django.db import Error
from django.db import DatabaseError
from django.db import DataError
from datetime import datetime
from datetime import date
from datetime import timedelta
from twilio.rest import TwilioRestClient
import sendalert


#application specific
from cityalertdata.models import *
from subscriber.models import *
from decisionengine.models import *
from sendalert.models import * 



@shared_task(name='sendalert.tasks.daily_frequency_alert')
def processData():
    
    """
    Name: sendalert.tasks.daily_frequency_alert Function: Scheduled task runs onces a day and populates the task queue with the alerts that need to be sent out
    based on predetermined frequency.

    1) Get all the relevant entries from last 24 hours.
    2) Populate the task queue with the alerts that need to be send out.  
    """
    try:
        alert = {}
        # Preparing a filter for date parameter to include objects entered in last 24 hours.
        datesearch = datetime.today() - timedelta(1)
        relevant_send_alert = SendAlertFrequency.objects.filter(date__gte = datesearch)
     
        if relevant_send_alert:
            # if relevant alerts are found then create a dictionary with address as the key and list of events as the values.
            for i in relevant_send_alert:
                if alert.has_key(i.contact):
                    alert[i.contact].append(i.event) 
                else:
                    alert[i.contact] = [i.event] 
            dt_today = date.today().isoformat() 
            for key,value in alert.items():
                # the len of the event list is checked to alert based on frequency.
                if value.__len__() >= 2:
                    for val in value:
                        #  Add a task to the taskqueue to send an alert 
                        sendalert.tasks.send_this_alert.delay(key.phone,dt_today + val.full_text)
        else: 
            raise DataError("No Alerts for today")
    except DataError, e:
        print e
    except Error, e:
        print e
    finally:
        print "Completed processing daily alerts for date:  %s " % (datesearch)
       

def get_new_event():
    try:
        new_event = Event.objects.filter(processed=False).earliest('id')
        return new_event
    except ObjectDoesNotExist:
        return None 

@shared_task(name='sendalert.tasks.send_this_alert')
def send_this_alert(phone,text):
    
    """
    Name: sendalert.tasks.send_this_alert, Type:Async Task, Function: Reads from a taskqueue and sends out the alert via sms using twilio. 

    Requirements: 
        1) Twilio phoneaccount should be set up
        2) The sendalert.settings table should have the Twilio Auth Token and SID information
        3) Caching Subsystem should be enabled
    
    1) Read a task from task queue and send the alert to relevant user.

    """
    try:
        # Try to get twilio settings from the cache
        account_sid = cache.get('twilio_sid')
        auth_token = cache.get('twilio_token')
        fromph = cache.get('twilio_phone')

        # if any one of the setting is not cached cache all the enteries
        if (not account_sid) or (not auth_token) or (not fromph):
           try:
              print " caching twilio settings "
              setting_obj  = setting.objects.filter(vendor='twilio')
              print "setting_obj ", setting_obj
              if setting_obj:
                print "setting_obj[0] ", setting_obj[0]
                account_sid = setting_obj[0].account_sid
                auth_token = setting_obj[0].auth_token
                fromph = setting_obj[0].fromph
                cache.set('twilio_sid',account_sid)
                cache.set('twilio_token',auth_token)
                cache.set('twilio_phone',fromph)
              else:
                  print " Before raising error due to empty setting_obj "
                  raise Error
           except:
              print " Could not populate the twilio settings "
              raise Error
        
        client = TwilioRestClient(account_sid, auth_token)
        messg = client.messages.create(body=text, to=phone, from_=fromph)
        print messg
    except Error:
        print "Could not post the message to twilio %s , %s " % (phone, text)
    finally:
        print " Send sms to  %s the txt %s " % (phone, text) 

"""This module provides useful django admin hooks that allow you to manage
various components through the django admin panel (if enabled).
"""
from django.contrib import admin
from sendalert.models import setting

class setting_Admin(admin.ModelAdmin):
    """This class provides admin panel integration for our
    :class:`django_twilio.models.Caller` model.
    """
    list_display = ('__unicode__',)

admin.site.register(setting, setting_Admin)

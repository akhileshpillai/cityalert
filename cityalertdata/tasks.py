#standard lib
from __future__ import absolute_import
import logging,sys
import re
import sys
import dateutil.parser


# third party lib
from celery import shared_task
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist 
from django.conf import settings
from django.contrib.gis.geos import Point
from django.core.cache import cache
import json
import urllib

#application specific
from cityalertdata.models import Call_Code
from cityalertdata.models import Event


@shared_task(name='cityalertdata.tasks.getData')
def getData():
    """
    Name: cityalertdata.tasks.getData, Function: Populates the event table. The event table is eventually used by decisionengine and sendalert apps.
    ****** The following current method is only applicable to pull data methods wherein we pull data. However the datasource in our case does not provide incremental data as of now.

    1) Read data from the datasource.
    2) If only text address is provided then convert that address to lat/long and store the geo point(provided by django.contrib.gis.geos).
    3) Otherwise directy store the lat/long --> This is not tested though the code is provided below, since our data has text address and no lat/lng.
    4) Currently some flags are being read from settings.py file. However, we should store all these in database. Example in the sendalert.models.setting
    5) Also Caching framework should be enabled.

    """
    try:
        print "get data task begin"
        key = settings.GOOGLE_API_KEY
        resultsdict={}
        errordict={}
        
        #url = 'https://data.openjerseycity.org/api/action/datastore_search_sql?sql=SELECT * from "6cb5d0a3-b561-4d99-b4f4-dbc8f9ed9d89"'
        url = 'https://data.openjerseycity.org/api/action/datastore_search?resource_id=6cb5d0a3-b561-4d99-b4f4-dbc8f9ed9d89&limit=20'
    
        json_response = urllib.urlopen(url).read()
        if json.loads(json_response)["success"]:
            call_data_json=json.loads(json_response)["result"]["records"]
            print "Read data into call_data_json" 
        else:
            print "IOError" 
            raise IOError
    
        for i in call_data_json:
            print "Iterating through the data : ", i 
            # Populate the necessary parameters for the function insert_to_db
            call_code = i['CALLCODE'] # look up in cache 
            call_code_desc = i['Call Code Description']
            cv = cache.get(call_code) # Try the cache to get the call_code
            
            if not cv:
                try:
                    call_code_obj, created = Call_Code.objects.get_or_create(call_code=call_code,defaults={'call_code_desc':call_code_desc,'part_of_ui': False })
                except:
                    print " Could not populate the Call_Code: ",i
                    continue
                cv = call_code_obj
                call_code_desc = call_code_obj.call_code_desc
                cache.set(call_code,cv)

            call_code = cv
            tr = convert_to_dt(i['TR']) 
            
            if settings.ADDRESS:
                #  If only text address is provided then convert that address to lat/long and store the geo point(provided by django.contrib.gis.geos).
                print "ADDRESS=True"
                full_address = i["Full Address"]
                url = 'https://maps.googleapis.com/maps/api/geocode/json?key=%s&address=%s' % (key,full_address)
                response = urllib.urlopen(url).read()
                g = json.loads(response)
                if g["status"] == 'OK':
                    print "latitude and longitude returned by:  g[results][0][geometry][location]"
                    latitude = g["results"][0]["geometry"]["location"]["lat"] 
                    longitude = g["results"][0]["geometry"]["location"]["lng"]
                else:
                    print "Default longitude and latitude "
                    longitude = settings.CITY_LNG
                    latitude =  settings.CITY_LAT
                    
            else:
                # Otherwise directy store the lat/long --> This is not tested though the code is provided below, since our data has text address and no lat/lng.
                print "ADDRESS=False"
                latitude = i["latitude"]
                longitude = i["longitude"]
                addresslookup = "https://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&key=%s" % (lat,lng,key)
                response = urllib.urlopen(url).read()
                a = json.loads(response)
                if a["status"] == 'OK':
                    if a["results"][0]:
                        print a["results"][0]
                        full_address = a["results"][0]["formatted_address"]
                    else:
                        full_address = "Jersey City, NJ"
                        
            geolocation =  Point(latitude, longitude)
            geolocation.srid = 32140
                      
            #full_text = "On %s Following Alert Code: %s described in records as %s was received for %s " % (tr.ctime(),call_code.call_code,call_code.call_code_desc,full_address) 
            full_text = "On %s Following Alert %s was received for %s " % (tr.ctime(),call_code.call_code_desc,full_address) 
            dbinsert = insert_to_db(tr,call_code, geolocation, full_text) 
            if not dbinsert:
                continue

        print "get data task end"
    except IOError:
        print "Could not load data from the API : json_response = urllib.urlopen(url).read()"

def convert_to_dt(event_ts):
    return dateutil.parser.parse(event_ts)

def insert_to_db(tr,call_code, geolocation, full_text):
    try:
        print "tr,call_code, geolocation, full_text : %s ,%s, %s,%s " % (tr,call_code, geolocation, full_text) 
        obj, newlycreated = Event.objects.get_or_create(call_code=call_code,tr=tr, geolocation=geolocation,defaults={'full_text':full_text, 'processed':False})
        print " newly created:%s " % (newlycreated)
        if not newlycreated:
            print("A new Events document was NOT added")
        return 1
    except ObjectDoesNotExist:
        print("Some problem with Events document updates .")        
        return None 


@shared_task(name='cityalertdata.tasks.testTask')
def testTask():
    print "you are right "

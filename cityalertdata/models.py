from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.core.cache import get_cache
from django.core.cache import cache

# Create your models here.
class Call_Code(models.Model):
    call_code = models.CharField(max_length=255,unique=True,)
    call_code_desc = models.CharField(max_length=255,)
    part_of_ui = models.BooleanField(default=False,)
    objects = models.GeoManager()

    def __unicode__(self):
        name = str(self.call_code) + " " + str(self.call_code_desc) 
        if self.part_of_ui:
            name += ' : (UI)'
        return name

class Event(models.Model):
    call_code = models.ForeignKey(Call_Code)
    tr = models.DateTimeField(blank=False,) 
    full_text = models.CharField(max_length=512,blank=False,) 
    geolocation = models.PointField(srid=32140)
    processed = models.BooleanField(default=False, blank=False,)
    objects = models.GeoManager()



def cache_cc(sender,instance, created, **kwargs):
   key = instance 
   try:
       print "Trying to get the following" , key
       cache_obj = Call_Code.objects.get(id=key.id)
       cache.set(key.call_code,cache_obj,None)
   except ObjectDoesNotExist:
       print "The call_code to be added to cache does not exist in database"


post_save.connect(cache_cc, sender=Call_Code, dispatch_uid="update_cache_cc")

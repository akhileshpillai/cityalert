"""This module provides useful django admin hooks that allow you to manage
various components through the django admin panel (if enabled).
"""
from django.contrib import admin
from cityalertdata.models import Call_Code

class Call_Code_Admin(admin.ModelAdmin):
    """This class provides admin panel integration for our
    :class:`django_twilio.models.Caller` model.
    """
    list_display = ('__unicode__',)

admin.site.register(Call_Code, Call_Code_Admin)

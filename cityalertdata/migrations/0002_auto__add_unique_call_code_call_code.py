# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Call_Code', fields ['call_code']
        db.create_unique(u'cityalertdata_call_code', ['call_code'])


    def backwards(self, orm):
        # Removing unique constraint on 'Call_Code', fields ['call_code']
        db.delete_unique(u'cityalertdata_call_code', ['call_code'])


    models = {
        u'cityalertdata.call_code': {
            'Meta': {'object_name': 'Call_Code'},
            'call_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'call_code_desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part_of_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'cityalertdata.event': {
            'Meta': {'object_name': 'Event'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'full_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'geolocation': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tr': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['cityalertdata']
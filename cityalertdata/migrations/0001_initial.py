# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Call_Code'
        db.create_table(u'cityalertdata_call_code', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('call_code', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('call_code_desc', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('part_of_ui', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'cityalertdata', ['Call_Code'])

        # Adding model 'Event'
        db.create_table(u'cityalertdata_event', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('call_code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cityalertdata.Call_Code'])),
            ('tr', self.gf('django.db.models.fields.DateTimeField')()),
            ('full_text', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('geolocation', self.gf('django.contrib.gis.db.models.fields.PointField')(srid=32140)),
        ))
        db.send_create_signal(u'cityalertdata', ['Event'])


    def backwards(self, orm):
        # Deleting model 'Call_Code'
        db.delete_table(u'cityalertdata_call_code')

        # Deleting model 'Event'
        db.delete_table(u'cityalertdata_event')


    models = {
        u'cityalertdata.call_code': {
            'Meta': {'object_name': 'Call_Code'},
            'call_code': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'call_code_desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part_of_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'cityalertdata.event': {
            'Meta': {'object_name': 'Event'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'full_text': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'geolocation': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tr': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['cityalertdata']
# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Event.processed'
        db.add_column(u'cityalertdata_event', 'processed',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Event.processed'
        db.delete_column(u'cityalertdata_event', 'processed')


    models = {
        u'cityalertdata.call_code': {
            'Meta': {'object_name': 'Call_Code'},
            'call_code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'call_code_desc': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'part_of_ui': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'cityalertdata.event': {
            'Meta': {'object_name': 'Event'},
            'call_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cityalertdata.Call_Code']"}),
            'full_text': ('django.db.models.fields.CharField', [], {'max_length': '512'}),
            'geolocation': ('django.contrib.gis.db.models.fields.PointField', [], {'srid': '32140'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'processed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'tr': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['cityalertdata']
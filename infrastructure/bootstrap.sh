#!/bin/bash -xv

DEV_MODE=1  # dev mode serves content from localhost instead of the vagrant image
SRC_PATH=$HOME/src/YOLO
APP_PATH=$SRC_PATH
APP_PORT=8000
GIT_HOST=bitbucket.org

function configure_os() {
  sudo apt-get update
  sudo apt-get -y install chkconfig
  sudo apt-get -y install git
  sudo apt-get -y install  python-dev python-setuptools python-pip
  sudo apt-get -y install libpq-dev
  sudo apt-get -y install vim tmux tree
  echo 'export PS1="\[\033[33m\][\u@\h \W]\$ \[\033[0m\]"' >> ~/.bash_profile
  sudo /sbin/iptables -I INPUT -p tcp -j ACCEPT --dport $APP_PORT  # open firewall port
  sudo mkdir ~/.ssh
  sudo cp /vagrant/sshconfig/* ~/.ssh/
  ssh-keyscan $GIT_HOST >> /root/.ssh/known_hosts
}


function configure_memcache() {
  sudo apt-get -y install memcached
  sudo /etc/init.d/memcached restart
}

function configure_postgresql() {
  sudo sh /vagrant/apt.postgresql.org.sh
  # sudo apt-get -y install pgadmin3
  sudo apt-get -y install postgresql-9.3 postgresql-9.3-postgis
  sudo apt-get autoremove -y
  export PATH=/usr/lib/postgresql/9.3/bin:$PATH
  export PGPASSWORD=postgres
  sudo chkconfig postgresql on
  #sudo service postgresql initdb
  
  echo "local   all all         trust" | sudo tee  /etc/postgresql/9.3/main/pg_hba.conf
  #echo "host    all  all     127.0.0.1/32 trust" | sudo tee -a /etc/postgresql/9.3/main/pg_hba.conf 
  sudo service postgresql restart
  
  cd /tmp  # don't complain about $pwd permissions when doing sudo -u
  
  createdb -U postgres citygeodb
  createlang -U postgres plpgsql citygeodb
  psql -U postgres -d citygeodb -c "create extension postgis";
  psql -U postgres -d citygeodb -c "create extension postgis_topology";
  psql -U postgres -d citygeodb -c "CREATE USER citygeodbuser WITH SUPERUSER PASSWORD 'citygeodbuser';"
  cd -

  #sudo apt-get -y install postgresql postgresql-client
  #psql -d yourdatabase -c "CREATE EXTENSION postgis;"
  #psql -d yourdatabase -c "CREATE EXTENSION postgis_topology;"
  #psql -d yourdatabase -c "CREATE EXTENSION postgis_tiger_geocoder;"
}

function configure_rabbitmq() {
  sudo apt-get -y install rabbitmq-server
  sudo chkconfig rabbitmq-server on
  sudo service rabbitmq-server start
  sudo rabbitmqctl add_user cityalertuser cityalertuser
  sudo rabbitmqctl add_vhost cityalerthost
  sudo rabbitmqctl set_permissions -p cityalerthost cityalertuser ".*" ".*" ".*"
}

function configure_redis() {
  wget http://download.redis.io/redis-stable.tar.gz
  tar xvzf redis-stable.tar.gz
  cd redis-stable
  sudo make
  sudo make test
  sudo make install
  sudo utils/install_server.sh -y
  sudo service redis_6379 restart
}

function configure_django() {
  mkdir -p $SRC_PATH
  git clone git@bitbucket.org:akhileshpillai/cityalert.git $SRC_PATH
  #mv $SRC_PATH/alert_app $SRC_PATH/cityalert
  cd $APP_PATH
  

  # sometimes pip fails with SSLError; retry a few times
  for i in 1 2 3; do
    sudo pip install -r /vagrant/requirements.pip && break || sleep 30
  done
  
  python manage.py schemamigration  cityauth --initial
  python manage.py schemamigration  cityalertdata  --initial
  python manage.py schemamigration  subscriber  --initial
  python manage.py schemamigration  decisionengine  --initial
  python manage.py schemamigration  allauth  --initial
  python manage.py syncdb --noinput
  python manage.py migrate allauth
  python manage.py migrate allauth.socialaccount
  python manage.py migrate cityauth
  python manage.py migrate cityalertdata
  python manage.py migrate subscriber
  python manage.py migrate decisionengine
#  #python manage.py syncdb --all --noinput

  psql -U citygeodbuser -d citygeodb -c "delete from socialaccount_socialapp"
  psql -U citygeodbuser -d citygeodb -c "delete from socialaccount_socialapp_sites"
  psql -U citygeodbuser -d citygeodb -c "insert into django_site (id,domain,name) values (1,'clusynk.com', 'cityalert')"

  #id | provider |     name      | key |              secret              |                                client_id                                 
  #----+----------+---------------+-----+----------------------------------+--------------------------------------------------------------------------
  #  2 | facebook | facebook_auth |     | 7b00169bbe56474b5933d6377d46cb5b | 533698230069240
  #  1 | google   | google_auth   |     | bVNpAICfH4tMHGDo_IFBA9T8         | 185837917685-r03okagcua0g2bc4it8n6kgv8lmg3lan.apps.googleusercontent.com
  
  #psql -U citygeodbuser -d citygeodb -c "
    #insert into socialaccount_socialapp (id, provider, name, client_id, secret, key)
    #values (1, 'google', 'google_auth', '6hG-3_61n00maUZAdbgtby4h','776495814807-8naqjtq8uad2tf9u5dt8qh0m17l6q5a6.apps.googleusercontent.com','')"
  psql -U citygeodbuser -d citygeodb -c "
    insert into socialaccount_socialapp (id, provider, name, client_id, secret, key)
    values (1, 'google', 'google_auth','185837917685-r03okagcua0g2bc4it8n6kgv8lmg3lan.apps.googleusercontent.com','6hG-3_61n00maUZAdbgtby4h','')"
  psql -U citygeodbuser -d citygeodb -c "
    insert into socialaccount_socialapp (id, provider, name, client_id, secret, key)
    values (2, 'facebook', 'facebook_auth','533698230069240','7b00169bbe56474b5933d6377d46cb5b','')"
  psql -U citygeodbuser -d citygeodb -c "insert into socialaccount_socialapp_sites (socialapp_id, site_id) values (1, 1), (2, 1)"

  # might want to also manually do: python manage.py createsuperuser --username=admin
}

function configure_YOLO() {
  [[ $DEV_MODE ]] && cd /vagrant || cd $APP_PATH
  echo "***** Serving YOLO celery and django from $PWD"
  python manage.py celery worker --beat --loglevel debug --logfile /tmp/celery.log &
  python manage.py runserver 0.0.0.0:$APP_PORT &> /tmp/django.log &
}

function main() {
  for component in os postgresql rabbitmq django redis memcache; do configure_$component; done
  echo "***** All done! For shell access to your vm type 'vagrant ssh'"
  echo "***** If everything worked, app should be running on localhost:$APP_PORT"
}

main

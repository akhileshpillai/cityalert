from django.conf import settings
 
 
def global_callcode_settings(request):
    # return any necessary values
    return {
        'CUSTOM_CALL_CODE': settings.CALL_CODE,
    }
